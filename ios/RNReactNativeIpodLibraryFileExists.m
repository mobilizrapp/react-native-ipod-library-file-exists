
#import "RNReactNativeIpodLibraryFileExists.h"
#import <AVFoundation/AVFoundation.h>

@implementation RNReactNativeIpodLibraryFileExists

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(exists:(NSString *)file
                  callback:(RCTResponseSenderBlock)successCallback
                  errorCallback:(RCTResponseSenderBlock)failureCallback) {
    [self iPodFileExists:file callback:successCallback errorCallback:failureCallback];
}

-(void)iPodFileExists:(NSString *)file callback:(RCTResponseSenderBlock)successCallback errorCallback:(RCTResponseSenderBlock)failureCallback
{
    if (![file containsString:@"ipod"]) {
        failureCallback(@[@"Not an iPod file"]);
        return;
    }
    
    NSURL *musicUrl = [NSURL URLWithString:file];
    
    if (![[AVAudioPlayer alloc] initWithContentsOfURL:musicUrl error:nil]) {
        failureCallback(@[@"false"]);
        return;
    }
    
    successCallback(@[@"true"]);
}


@end
