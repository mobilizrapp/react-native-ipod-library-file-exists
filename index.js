
import { NativeModules } from 'react-native';

const { RNReactNativeIpodLibraryFileExists } = NativeModules;

export default RNReactNativeIpodLibraryFileExists;
