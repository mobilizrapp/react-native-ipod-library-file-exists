using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace React.Native.Ipod.Library.File.Exists.RNReactNativeIpodLibraryFileExists
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNReactNativeIpodLibraryFileExistsModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNReactNativeIpodLibraryFileExistsModule"/>.
        /// </summary>
        internal RNReactNativeIpodLibraryFileExistsModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNReactNativeIpodLibraryFileExists";
            }
        }
    }
}
